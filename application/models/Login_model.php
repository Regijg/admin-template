<?php
class Login_model extends CI_Model {

function __construct (){
	parent ::__construct();
}


function getUser() {
    $query = $this->db->get('mytable');
}

function showDatapasien($param = array()) {
	if (isset($param)) {
		$param = ( object ) $param;
	}
	// Set Id
	if (isset($param->id)) {
		$setId = $param->id;
		$this->db->where ( 'pasien_head_id', $setId );
	}
	// Set Order
	if (isset($param->order)) {
		$setOrder = $param->order;
		$sortColumn = 'tp.insert_date';
		if (isset($param->order_column)) {
			$sortColumn = $param->order_column != NULL ? $param->order_column : $sortColumn;
		}
		$this->db->order_by ( $sortColumn, $setOrder );
	}

	// Search
	if (isset($param->search)) {
		$this->db->like($param->search);
	}

	$this->db->join ( 'trx_tagihan_ri_head trx', 'tp.pasen_id = trx.tagihan_ri_head_pasen_id');
	$this->db->join ( 'tbl_ruangan tr', 'tr.ruangan_id = trx.tagihan_ri_head_ruangan_id');
	$this->db->join ( 'trx_registrasi_ri tri', 'tp.pasen_id = tri.registrasi_ri_pasen_id');
	$this->db->select ( "
			tp.pasen_no_medrek,
			tp.pasen_gelar_depan||' '||tp.pasen_nama||' '||tp.pasen_gelar_belakang as nama_lengkap,
            tr.ruangan_nama||' / '|| tr.ruangan_kelas as ruang_kelas, 
            to_char(tri.registrasi_ri_tgl_masuk, 'DD Mon YYYY      HH:MI') 
	");
	$this->db->protect_identifiers=false;

	// Set Limit Offset
	if (isset($param->limit)) {
		$limitData = explode ( ',', $param->limit );
		return $this->db->get ( "tbl_pasen tp", $limitData [0] ,  $limitData [1]);
		
	} else {
		return $this->db->get ( "tbl_pasen tp" );
		
	}
}

}
?>