<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// require APPPATH . '/controllers/Widget.php';

/**
 * @author REGI.J.Gustansyah
 */
class Home extends CI_Controller {

	private $widget;
	public function __construct() {
		parent::__construct ();
		$this->load->helper('url');
		$this->load->helper('form');
	}

	/**
	 * Index view
	 * --------------------------------------------------------------------------------------------------------
	 */
	public function index() {
		$setting = array(
			'dashboard'=>"active"
		);
	 	$this->template->print_layout('dashboard',$setting);
	}

}
