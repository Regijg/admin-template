<div class="error-body text-center">
    <h1>404</h1>
    <h3 class="text-uppercase">Page Not Found !</h3>
    <p class="text-muted m-t-30 m-b-30">YOU SEEM TO BE TRYING TO FIND HIS WAY CLIENT LIST</p>
    <!-- <a href="index.html" class="">Back to home</a>  -->
    <button type="button" class="btn btn-info btn-rounded waves-effect waves-light m-b-40" id="btn-previous2" onclick="window.history.back();">Kembali</button>
</div>
<script>
require.config({
    baseUrl: baseURL+'tpl/md-admin/',
    urlArgs: "bust=" + (new Date()).getTime(),
    paths: {
        "core"               		:   'js/main',
        "jspage"					: 	'js/page/dashboard'
    }
});
require(["core"], function(core) {
    require([
             'jspage',
             'tpl.all'
     ], function(){});
});
</script>
