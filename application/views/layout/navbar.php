<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <div class="navbar-header" style="background:#1976d2">
            <!-- <a class="navbar-brand navstyle" href="<?= base_url('dashboard') ?>" >
                <img src="<?=static_file()?>assets/images/adakasir-logo.png" style="width: 50%;">
            </a> -->
            <b style="color:#fff">Test</b>
        </div>
        <div class="navbar-collapse">

            <ul class="navbar-nav mr-auto mt-md-0">
                <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
            </ul>

            <ul class="navbar-nav my-lg-0">
                 <!-- <li class="nav-item hidden-sm-down search-box"> <a class="nav-link hidden-sm-down text-muted waves-effect waves-dark" href="#">
                     <i class=" mdi mdi-email"></i>
                    </a>
                </li> -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="<?=static_file().'assets/images/'?>" alt="user" class="profile-pic" />
                    </a>
                    <div class="dropdown-menu dropdown-menu-right scale-up">
                        <ul class="dropdown-user">
                            <li>
                                <div class="dw-user-box">
                                    
                                </div>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?=base_url()?>account"><i class="fa fa-user"></i> My Profile</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?=base_url()?>login/logout"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>
