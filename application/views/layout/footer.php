<script src="<?=static_file()?>assets/plugins/bootstrap/js/popper.min.js"></script>
<script src="<?=static_file()?>assets/plugins/jquery/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-tagsinput/1.3.6/jquery.tagsinput.min.js"></script>
<script src="<?=static_file()?>assets/plugins/jqgrid/js/jquery.jqGrid.js"></script>
<script src="<?=static_file()?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=static_file()?>assets/plugins/bootstrap/js/bootstrap.notify.min.js"></script>
<script src="<?=static_file()?>assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?=static_file()?>assets/plugins/bootstrap-tag-input/bootstrap-tagsinput.min.js"></script>
<script src="<?=static_file()?>assets/plugins/jqgrid/js/grid.locale-en.js"></script>
<script src="<?=static_file()?>assets/plugins/select2/js/select2.full.min.js"></script>
<script src="<?=static_file()?>assets/plugins/multiselect/js/jquery.multi-select.js"></script>
<script src="<?=static_file()?>js/jquery.slimscroll.js"></script>
<script src="<?=static_file()?>js/waves.js"></script>
<script src="<?=static_file()?>js/sidebarmenu.js"></script>
<script src="<?=static_file()?>assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
<script src="<?=static_file()?>assets/plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="<?=static_file()?>js/jq-mask/maskMoney.js"></script>
<script src="<?=static_file()?>js/swal-alert.js"></script>
<script src="<?=static_file()?>js/custom.min.js"></script>

<script src="<?=static_file()?>assets/plugins/moment/moment.js"></script>
<script src="<?=static_file()?>assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
<script src="<?=static_file()?>assets/plugins/skycons/skycons.js"></script>
<script src="<?=static_file()?>assets/plugins/chartist-js/dist/chartist.min.js"></script>
<script src="<?=static_file()?>assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
<script src="<?=static_file()?>js/dashboard.js"></script>
<script src="<?=static_file()?>js/connection.js"></script>
<script src="<?=static_file()?>js/base64.js"></script>
<?php if (isset($js_file)): ?>
<?php foreach ($js_file as $key): ?>
<script src="<?=static_file()?>js/<?=$key?>"></script>
<?php endforeach; ?>
<?php endif; ?>