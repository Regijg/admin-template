<aside class="left-sidebar">
  <!-- Sidebar scroll-->
  <div class="scroll-sidebar">

    <nav class="sidebar-nav">
      <ul id="sidebarnav">

        <li>
          <a class="waves-effect waves-dark" href="<?= base_url('home') ?>">
            <i class="mdi mdi-home"></i>
            <span class="hide-menu">Dashboard </span>
          </a>
        </li>
      </ul>
    </nav>
  </div>
</aside>