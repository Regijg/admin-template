<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<meta  name="base_url" content="<?=base_url()?>">
<link rel="icon" type="image/png" sizes="16x16">
<title>.:: ADA KASIR ::.</title>
<link href="<?=static_file()?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="<?=static_file()?>assets/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet"/>
<link href="<?=static_file()?>assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet">
<link href="<?=static_file()?>assets/plugins/bootstrap-tag-input/bootstrap-tagsinput.css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" />
<link href="<?=static_file()?>assets/plugins/jquery/css/jquery.tagsinput.min.css" rel="stylesheet">
<link href="<?=static_file()?>assets/plugins/jqgrid/css/ui.jqgrid-bootstrap.css" rel="stylesheet">
<link href="<?=static_file()?>assets/plugins/jquery/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="<?=static_file()?>assets/plugins/select2/css/select2.min.css" rel="stylesheet">
<link href="<?=static_file()?>assets/plugins/multiselect/css/multi-select.css" rel="stylesheet"/>
<link href="<?=static_file()?>css/formValidation.css" rel="stylesheet">
<link href="<?=static_file()?>assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
<link href="<?=static_file()?>assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet">
<link rel="stylesheet" href="<?=static_file()?>css/swal-alert.css">
<link href="<?=static_file()?>assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">

<link href="<?=static_file()?>assets/plugins/dropify/dist/css/dropify.min.css" rel="stylesheet">

<link href="<?=static_file()?>css/style.css" rel="stylesheet">
<link href="<?=static_file()?>css/color.css" rel="stylesheet">
<link href="<?=static_file()?>css/custom.css" rel="stylesheet">
<link href="<?=static_file()?>css/modal.css" rel="stylesheet">
<script src="<?=static_file()?>assets/plugins/jquery/jquery.min.js"></script>
<script src="<?=static_file()?>assets/plugins/jquery/jquery.form.min.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="<?=static_file()?>js/formValidation.js"></script>
<script src="<?=static_file()?>js/framework/bootstrap.js"></script>
<script src="<?=static_file()?>assets/plugins/dropify/dist/js/dropify.min.js"></script>
