<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('static_file')) {

    function static_file() {
        $CI = & get_instance();

        return $CI->config->item('static_file');
    }

}

if (!function_exists('to_mysql_date')) {

    function to_mysql_date($tanggal) {
        return date_format(date_create($tanggal),'Y-m-d');
    }

}

if (!function_exists('to_human_date')) {

    function to_human_date($tanggal) {
        return date_format(date_create($tanggal),'m/d/Y');
    }

}

if (!function_exists('c_link')) {

    function c_link($nama, $id, $link) {
        return "<a href='".base_url().$link."/view/".$id."'>$nama</a>";
    }

}

if (!function_exists('int_to_rupiah')) {

    function int_to_rupiah($nilai) {
        return "Rp. ".number_format($nilai,0,',','.');
    }

}

if(!function_exists('rupiah_to_int'))
{
  function rupiah_to_int($number)
    {
        $int = $number;
        if($number != ""){
          $data = explode(',',$number);
          $int  = str_replace('.','',$data[0]);
        }

        return $int;
    }
}

if (!function_exists('status_paket')) {

    function status_paket($status) {
      if($status){
        $label = '<span class="label label-success">Aktif</span>';
      }else{
        $label = '<span class="label label-danger">Tidak Aktif</span>';
      }
        return $label;
    }

}

if (!function_exists('status_trx')) {

    function status_trx($status) {
      if($status=="daftar"){
        $label = '<span class="label label-info">'.$status.'</span>';
	  }elseif($status=="batal"){
        $label = '<span class="label label-danger">'.$status.'</span>';
      }else{
        $label = '<span class="label label-success">'.$status.'</span>';
      }
        return $label;
    }

}

if (!function_exists('status_user')) {

    function status_user($status) {
      if($status=="pending"){
        $label = '<span class="label label-danger">'.$status.'</span>';
    }elseif($status=="aktif"){
        $label = '<span class="label label-success">'.$status.'</span>';
      }else{
        $label = '<span class="label label-warning">'.$status.'</span>';
      }
        return $label;
    }

}

if (!function_exists('aksi')) {
    function aksi($id, $link, $view = "false", $edit = "false", $hapus="false") {

	  $format = "";
	
  if($view=="true"):
	  $format .= "".
          "<a title='Lihat' class='btn btn-white btn-sm' href='".base_url().$link."/view/".$id."'>
              <i class='material-icons'>assignment</i>
          </a>";
	endif;

	if($edit=="true"):
      $format .= "".
          "<a title='Edit' class='btn btn-white btn-sm' href='".base_url().$link."/edit/".$id."'>
              <i class='material-icons'>assignment_turned_in</i>
          </a>";
	endif;

	if($hapus=="true"):
      $format .= "".
          "<a title='Hapus' class='btn btn-white btn-sm' href='#' onclick='delConf(\"".base_url().$link."/delete/".$id."\")'>
              <i class='material-icons'>delete</i>
          </a>";
	endif;

      return "<center>".$format."</center>";

    }
}